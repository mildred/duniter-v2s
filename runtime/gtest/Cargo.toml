[build-dependencies.substrate-wasm-builder]
git = 'https://github.com/duniter/substrate'
branch = 'duniter-substrate-v0.9.23'

[package]
authors = ['Axiom-Team Developers <https://axiom-team.fr>']
edition = "2021"
homepage = 'https://substrate.dev'
license = 'AGPL-3.0'
name = 'gtest-runtime'
repository = 'https://git.duniter.org/nodes/rust/duniter-v2s'
version = '3.0.0'
[package.metadata.docs.rs]
targets = ['x86_64-unknown-linux-gnu']

[features]
default = ['std']
runtime-benchmarks = [
    'frame-benchmarking',
    'frame-support/runtime-benchmarks',
    'frame-system-benchmarking',
    'frame-system/runtime-benchmarks',
    'hex-literal',
    'pallet-balances/runtime-benchmarks',
    'pallet-identity/runtime-benchmarks',
    'pallet-treasury/runtime-benchmarks',
    'pallet-universal-dividend/runtime-benchmarks',
    'common-runtime/runtime-benchmarks',
    'sp-runtime/runtime-benchmarks',
]
std = [
    'codec/std',
    'frame-executive/std',
    'frame-support/std',
    'frame-system-rpc-runtime-api/std',
    'frame-system/std',
    "frame-try-runtime/std",
    'pallet-atomic-swap/std',
    'log/std',
    'pallet-authority-discovery/std',
    'pallet-authority-members/std',
    'pallet-babe/std',
    'pallet-balances/std',
    'pallet-certification/std',
    'pallet-collective/std',
    'pallet-duniter-test-parameters/std',
    'pallet-duniter-account/std',
    'pallet-duniter-wot/std',
    'pallet-grandpa/std',
    'pallet-identity/std',
    'pallet-membership/std',
    'pallet-provide-randomness/std',
    'pallet-im-online/std',
    'pallet-multisig/std',
    'pallet-preimage/std',
    'pallet-proxy/std',
    'pallet-session/std',
    'pallet-sudo/std',
    'pallet-universal-dividend/std',
    'pallet-upgrade-origin/std',
    'pallet-timestamp/std',
    'pallet-transaction-payment-rpc-runtime-api/std',
    'pallet-transaction-payment/std',
    'pallet-treasury/std',
    'common-runtime/std',
    'serde',
    'sp-api/std',
    'sp-arithmetic/std',
    'sp-authority-discovery/std',
    'sp-block-builder/std',
    'sp-consensus-babe/std',
    'sp-core/std',
    'sp-inherents/std',
    'sp-offchain/std',
    'sp-membership/std',
    'sp-runtime/std',
    'sp-session/std',
    'sp-std/std',
    'sp-transaction-pool/std',
    'sp-version/std',
]
try-runtime = [
    "frame-executive/try-runtime",
    "frame-try-runtime",
    "frame-system/try-runtime",
    "pallet-authority-discovery/try-runtime",
    "pallet-authorship/try-runtime",
    "pallet-balances/try-runtime",
    "pallet-transaction-payment/try-runtime",
    "pallet-collective/try-runtime",
    "pallet-grandpa/try-runtime",
    "pallet-im-online/try-runtime",
    "pallet-multisig/try-runtime",
    "pallet-offences/try-runtime",
    "pallet-proxy/try-runtime",
    "pallet-scheduler/try-runtime",
    "pallet-session/try-runtime",
    "pallet-timestamp/try-runtime",
    "pallet-treasury/try-runtime",
    "pallet-babe/try-runtime",
    "pallet-utility/try-runtime",
]

[dev-dependencies]
sp-consensus-vrf = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23' }
sp-finality-grandpa = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23' }
sp-io = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23' }
sp-keyring = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23' }

[dependencies]
# local
common-runtime = { path = "../common", default-features = false }
pallet-authority-members = { path = '../../pallets/authority-members', default-features = false }
pallet-certification = { path = '../../pallets/certification', default-features = false }
pallet-duniter-test-parameters = { path = '../../pallets/duniter-test-parameters', default-features = false }
pallet-duniter-account = { path = '../../pallets/duniter-account', default-features = false }
pallet-duniter-wot = { path = '../../pallets/duniter-wot', default-features = false }
pallet-identity = { path = '../../pallets/identity', default-features = false }
pallet-membership = { path = '../../pallets/membership', default-features = false }
pallet-provide-randomness = { path = '../../pallets/provide-randomness', default-features = false }
pallet-universal-dividend = { path = '../../pallets/universal-dividend', default-features = false }
pallet-upgrade-origin = { path = '../../pallets/upgrade-origin', default-features = false }
sp-membership = { path = '../../primitives/membership', default-features = false }

# crates.io
codec = { package = "parity-scale-codec", version = "3.1.5", features = ["derive"], default-features = false }
log = { version = "0.4.17", default-features = false }
hex-literal = { version = '0.3.1', optional = true }
scale-info = { version = "2.1.1", default-features = false, features = ["derive"] }
serde = { version = "1.0.101", optional = true, features = ["derive"] }

# substrate
frame-benchmarking = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', optional = true }
frame-try-runtime = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false, optional = true }
frame-executive = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
frame-support = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
frame-system = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
frame-system-benchmarking = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', optional = true }
frame-system-rpc-runtime-api = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false}
pallet-atomic-swap = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-authority-discovery = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-authorship = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-babe = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-balances = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-collective = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-grandpa = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-im-online = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-offences = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-multisig = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-preimage = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-proxy = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-scheduler = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-session = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-sudo = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-timestamp = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-transaction-payment = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-transaction-payment-rpc-runtime-api = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-treasury = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
pallet-utility = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-api = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-arithmetic = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-authority-discovery = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-block-builder = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-consensus-babe = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-core = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-inherents = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-offchain = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-runtime = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-session = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-std = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-transaction-pool = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
sp-version = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.23', default-features = false }
